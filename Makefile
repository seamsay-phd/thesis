PUBLIC ?= public

RUBBER ?= rubber

SRC := src
BUILD := build
NAME := Thesis

.PHONY: all
all: pdf

.PHONY: pdf
pdf: $(BUILD)/$(NAME).pdf

.PHONY: install
install: install-pdf

.PHONY: install-pdf
install-pdf: $(BUILD)/$(NAME).pdf
	install -d $(PUBLIC)
	install $(BUILD)/$(NAME).pdf $(PUBLIC)

$(BUILD)/$(NAME).bib: $(SRC)/$(NAME).bib
	install -d $(BUILD)
	install $< $@

$(BUILD)/$(NAME).tex: $(SRC)/$(NAME).tex
	install -d $(BUILD)
	install $< $@

$(BUILD)/$(NAME).pdf: $(BUILD)/$(NAME).tex $(BUILD)/$(NAME).bib
	$(RUBBER) --into $(BUILD) --unsafe --module xelatex $(RUBBER_FLAGS) $<

.PHONY: clean
clean:
	$(RM) -r $(BUILD) $(PUBLIC) result
