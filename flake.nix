{
  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs";
  };

  outputs = {
    self,
    flake-utils,
    nixpkgs,
  }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};

      tex = pkgs.texlive.combine {
        inherit (pkgs.texlive) scheme-basic biblatex bibtex csquotes xetex;
      };

      src = ./.;

      buildInputs = with pkgs; [biber rubber tex];
      installFlags = ["PUBLIC=$(out)"];
    in {
      packages = {
        default = self.packages.${system}.pdf;

        pdf = pkgs.stdenv.mkDerivation {
          pname = "Thesis-PDF";
          version = "0.0.0";

          inherit src buildInputs installFlags;
        };
      };

      devShells.default = pkgs.mkShell {
        inherit src buildInputs installFlags;
      };

      formatter = pkgs.alejandra;
    });
}
