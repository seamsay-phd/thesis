\documentclass[a4,11pt]{report}

% NOTE: The Babel package must always come first.
\usepackage[british]{babel}

\usepackage{csquotes}

\usepackage[sorting=none]{biblatex}
\addbibresource{Thesis.bib}

\title{Thesis}
\author{Sean Marshallsay \\ smarshallsay01@qub.ac.uk}
\date{\today}

\begin{document}
    \maketitle

    \begin{displayquote}[Terry Pratchett, \textit{Monstrous Regiment}]
        People build something that works.
        Then circumstances change, and they have to tinker with it to make it continue to work, and they are so busy tinkering that they cannot see that a much better idea would be to build a whole new system to deal with the new circumstances.
        But to an outsider, the idea is obvious.
    \end{displayquote}


    \chapter{Holdover From Refactor Background}

    When developing the theory for a mathematical model, it is common (arguably required) to group together the data of the model based on what quantity they represent.
    For example, a Hamiltonian for a group of \(N\) particles interacting gravitationally might be written as
    \[
        \mathcal{H} = \frac{1}{2} \sum_{i = 1}^N m_i v_i^2 - \sum_{i = 1}^N \sum_{j = i+1}^N \frac{G m_i m_j}{\left|r_i - r_j\right|} \, .
    \]
    The data in this model are the masses (\(m_i\)), velocities (\(v_i\)), and positions (\(r_i\)) of the particles.
    When developing a scientific code it is common to translate this ontology directly to the software, storing separately a list of masses, velocities, and positions.
    In software, however, sticking too strictly to this flattened style of data can have negative consequences for both performance and comprehension.
    Instead it might make more sense to store a list of structured types representing particles, storing the mass, velocity, and position for each particle.
    This direct translation of mathematical ontologies into software is a prime pathway by which unstructured data makes its way into scientific code.

    An obvious example of this in RMT is the way the input Hamiltonian data is flattened in the code.
    Conceptually the Hamiltonian upon which RMT operates is split into symmetries, with each of these symmetries having their own data such as eigenfunctions or surface amplitudes.
    Indeed this is how the data is laid out in the input files which RMT consumes, there is a list of symmetry blocks and each of these blocks has inside it the data relating to that symmetry.
    In the mathematical theory upon which RMT is based, however, this ontology is inverted; there is a single object representing eigenfunctions.
    It was natural, therefore, to use this same flattened set of data when writing the code for RMT.

    However converting from the structured data in the input files to the unstructured data in RMT causes major friction.
    The eigenfunction for each symmetry block is a one-dimensional vector of real numbers, however when RMT reads the data it stores all the eigenfunctions for all the symmetry blocks in a matrix, with a column for each symmetry block.
    However the number of states in each symmetry block is not necessarily the same meaning that each column of the matrix might have a different number of valid elements, and so a second vector must be passed around alongside this matrix indicating how many values are in each column.
    The effect of this is that every time the eigenfunction matrix needs to be accessed extra logic must be performed to ensure that the correct elements of the matrix are being accessed.
    As this extra logic is difficult to abstract away, it imposes a cognitive overhead and problems with repeated logic~\parencite{ousterhout_philosophy_2018}.

    Another way that the unstructured data in RMT causes problems is in the work other programs have to do to pass data to RMT.
    RMT is a code for simulating the electron dynamics of a target system, and to do that it requires data about the electronic states and dipole couplings of that system.
    However RMT does not provide this data itself, instead it relies on other codes implementing the time-\emph{independent} R-matrix paradigm (currently RMatrixI, RMatrixII, or UKRmol+) to calculate and provide this data.
    The way that RMT receives this data is via input files; a program such as RMatrixII will generate a set of input files that RMT can consume.
    However due to the lack of a canonical structure to the data in RMT, there is no way for these programs to know how best to structure the data they export.
    This has led to each program exporting its data in a slightly different way, leading to RMT having to read each programs data in a different way.
    Right now that means that there are six different places among four different codebases where logic for exporting and importing data exists, if a new program were to implement support for RMT then those numbers could increase to eight and five respectively.

    Unstructured data is also responsible for RMT's over-reliance on global state.
    Due to this flattening of data RMT has many variables holding the state of the program, potentially many more than is actually necessary.
    Since procedures taking many local arguments are unwieldy to work with, most of the data is RMT is passed around in global arrays.
    This it makes it difficult to understand the flow of the program, many of these global variables are used in most modules of the code with no clear ownership.
    But it also makes isolating the code much more difficult.
    In order to run any function (that uses global state) you either need to run the application as a whole, or you need to setup the global state correctly for that function before running it and restore the original state after running the function.
    This in turn makes unit testing procedures in RMT much more difficult.

    The final example of how unstructured data causes issues in RMT is through discouraging documentation.
    Since the unstructured data is spread around the code, there is often no single place to document the data or single objects that represents the objects in the data.
    This means that documentation for the data has to be written externally and is therefore more prone to falling out of sync or even just being forgotten about.
    The unstructured data also means that the code is less likely to be self-documenting.
    The link between a data variable in the program and it's equivalent in the input data is hidden deep in the logic of a function and therefore difficult for a newcomer to find.

    Some of the symptoms of these problems include:
    \begin{itemize}
	\item Developers constantly having to relearn or refamiliarise themselves with significant portions of the codebase.
	\item New features or bug fixes requiring changes to many parts of the codebase, instead of being isolated to one or two areas.
	\item Development time for new features being dominated by time spent finding and fixing bugs.
	\item Excessively long on-boarding times for new developers.
	\item Difficulty writing comprehensive and useful unit tests.
    \end{itemize}

    \printbibliography
\end{document}
